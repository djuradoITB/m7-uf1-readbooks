package cat.itb.readbooks;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class BookFragment extends Fragment {
    private EditText editTextTitle;
    private EditText editTextAuthor;
    private Spinner spinnerStatus;
    private CheckBox checkBoxLlegit;
    private TextView textViewValoracio;
    private RatingBar ratingBar;
    private Button buttonAdd;
    private Book book;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.book_fragment, container, false);

        editTextTitle = view.findViewById(R.id.editTextFragTitle);
        editTextAuthor = view.findViewById(R.id.editTextFragAuthor);
        spinnerStatus = view.findViewById(R.id.spinnerFragStatus);
        checkBoxLlegit = view.findViewById(R.id.checkBoxFragIsRead);
        textViewValoracio = view.findViewById(R.id.textViewFragRating);
        ratingBar = view.findViewById(R.id.ratingBarFrag);
        buttonAdd = view.findViewById(R.id.buttonFragAdd);

//        Adaptador per al Spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.spinnerFragStatusSelection, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerStatus.setAdapter(adapter);

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Llibre afegit");
                String msg = "S'ha afegit el llibre: " + book.getTitle() +
                        "\nDe l'/la autor/a: " + book.getAuthor() +
                        "\nEl qual es trova: " + book.getStatus();
                msg += book.isRead() ? "\nI s'ha llegit" : "\nI no s'ha llegit";
                builder.setMessage(msg);
                BookViewModel.books.add(book);
                builder.show();
            }
        });

        if (getArguments() != null) {
            book = getArguments().getParcelable("book");
        }
        if (book != null) {
            editTextTitle.setText(book.getTitle());
            editTextAuthor.setText(book.getAuthor());
            spinnerStatus.setSelection(0);
            checkBoxLlegit.setChecked(book.isRead());
            ratingBar.setRating(book.getRating());
        } else {
            book = new Book();
        }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        editTextTitle.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                book.setTitle(editTextTitle.getText().toString());
                return false;
            }
        });

        editTextAuthor.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                book.setAuthor(editTextAuthor.getText().toString());
                return false;
            }
        });

        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                book.setStatus(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                book.setStatus("");
            }
        });

        checkBoxLlegit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                book.setRead(isChecked);
                if (book.isRead()) {
                    textViewValoracio.setVisibility(View.VISIBLE);
                    ratingBar.setVisibility(View.VISIBLE);
                } else {
                    textViewValoracio.setVisibility(View.INVISIBLE);
                    ratingBar.setVisibility(View.INVISIBLE);
                }
            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                book.setRating(ratingBar.getRating());
            }
        });
    }
}
