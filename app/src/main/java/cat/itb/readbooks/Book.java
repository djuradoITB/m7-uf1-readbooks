package cat.itb.readbooks;

import android.os.Parcel;
import android.os.Parcelable;

public class Book implements Parcelable {
    private String title;
    private String author;
    private String status;
    private boolean isRead;
    private float rating;

    public Book() {
        this.title = "";
        this.author = "";
        this.status = "";
        this.isRead = false;
        this.rating = 0;
    }

    public Book(String title, String author, String status, boolean isRead, float rating) {
        this.title = title;
        this.author = author;
        this.status = status;
        this.isRead = isRead;
        this.rating = rating;
    }


    protected Book(Parcel in) {
        title = in.readString();
        author = in.readString();
        status = in.readString();
        isRead = in.readByte() != 0;
        rating = in.readFloat();
    }

    public static final Creator<Book> CREATOR = new Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel in) {
            return new Book(in);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(author);
        dest.writeString(status);
        dest.writeByte((byte) (isRead ? 1 : 0));
        dest.writeFloat(rating);
    }
}
