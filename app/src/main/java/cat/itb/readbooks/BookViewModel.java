package cat.itb.readbooks;

import android.content.res.Resources;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

public class BookViewModel extends ViewModel {
    static List<Book> books = new ArrayList<>();

    public BookViewModel() {
        String[] status = {"Per llegir", "Llegint", "Finalitzat"};
        for (int i = 1; i <= 10; i++) {
            Book book = new Book("Titol del llibre "+i, "Autor/a del llibre "+i, status[((int) (Math.random() * status.length))], false, 3);
            books.add(book);
        }
    }
}
